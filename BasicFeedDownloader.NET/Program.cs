﻿using System;
using System.Net;

namespace BasicFeedDownloader.NET
{
    class Program
    {
        static string url = "https://productdata.awin.com/datafeed/download/apikey/465bd9d67984a6f2ac248b35409e450a/language/en/fid/2518/columns/aw_deep_link,product_name,aw_product_id,merchant_product_id,merchant_image_url,description,merchant_category,search_price,merchant_name,merchant_id,category_name,category_id,aw_image_url,currency,store_price,delivery_cost,merchant_deep_link,language,last_updated,display_price,data_feed_id,brand_name,brand_id,colour,product_short_description,specifications,condition,product_model,model_number,dimensions,keywords,promotional_text,product_type,in_stock,ean,upc,mpn,product_GTIN/format/csv/delimiter/%2C/compression/gzip/adultcontent/1/";

        static void Main(string[] args)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Timeout = (int)TimeSpan.FromSeconds(30).TotalMilliseconds;
                webRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate; // Automatically decompress the stream when grabbing it

                if (webRequest == null)
                {
                    throw new Exception();
                }

                using (var webResponse = webRequest.GetResponse()){}
            }
            catch (WebException ex)
            {
                Console.WriteLine("An error has occured");
                Console.WriteLine(ex);
            }

            Console.WriteLine("We have downloaded the Feed, press any key to Exit");
            Console.ReadKey();
        }
    }
}
